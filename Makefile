#******************************************************************************
#
#      filename:  Makefile
#
#   description:	Git exercises
#
#        author:  Hermanson, Finn
#      login id:  FA_18_CPS444_13
#
#         class:  CPS 444
#    instructor:  Perugini
#    assignment:  Homework #5
#
#      assigned:  September 18, 2018
#           due:  September 27, 2018
#
#******************************************************************************

CC = gcc
C_FLAGS = -c
PGM = keeplog

all: $(PGM) 

$(PGM): $(PGM).o
	$(CC) -s -o $(PGM) $(PGM).o -lkeeplog_helper1 -lkeeplog_helper2 -llist

c.o:
	$(CC) $(C_FLAGS) $< -o $@

clean: 
	@ rm -f *.o
	@ rm -f $(PGM)
	@ echo Clean Complete

